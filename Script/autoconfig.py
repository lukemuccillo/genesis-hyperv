#!/usr/bin/python

import fcntl, socket, struct
import json
import urllib2
import os

webhost = "labcontrol.skycoder.com.au"
webhost_ip = "10.10.10.144"

if not os.geteuid()==0:
    sys.exit("\nOnly root can run this script\n")

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]

macAddr = getHwAddr('eth0')
hostname = socket.gethostname()

f = open("/etc/hosts", "r")
hosts_content = f.read()
f.close()

if hosts_content.find(webhost) == -1:
    f = open("/etc/hosts", "a")
    f.write(webhost_ip + "\t" + webhost + "\n")
    f.close()

url =  "http://" + webhost + "/VirtualMachine/GetDetailsByMacAddress?id=" + macAddr
print url

print hostname
data = json.load(urllib2.urlopen(url))

newHostname = data['Hostname']
newFqdn = data["FQDN"]

if newHostname != hostname and data['Status'] != "NETWORKED":
        print "Hostname needs to be updated"

        print "[+] Updating the /etc/hosts file"
        f = open("/etc/hosts", "r")     #open file for reading
        contents = f.read()             #read contents
        f.close()                       #close file

        replace = newFqdn + " " + newHostname
        newContent = contents.replace(hostname, replace)      #replace
        f = open("/etc/hosts", "w")     #open file for writing
        f.write(newContent)               #write the altered contents
        f.close()

        print "[+] Updating the hostname file"
        f_hostname = open("/etc/hostname", "w")
        f_hostname.write(newHostname + "\n")
        f_hostname.close()

        print "[+] Updating the puppet server IP address"
        f_puppet = open("/etc/hosts", "a")
        f_puppet.write(data['PuppetMaster'] + "\tpuppet\n")
        f_puppet.close()

        print "[+] Updating networking information"
        f_interfaces = open("/etc/network/interfaces", "w")
        f_interfaces.write("auto lo\n")
        f_interfaces.write("iface lo inet loopback\n\n")
        f_interfaces.write("auto eth0\n")
        f_interfaces.write("iface eth0 inet static\n")
        f_interfaces.write("address " + data["IPAddress"] + "\n")
        f_interfaces.write("netmask " + data["SubnetMask"] + "\n")
        f_interfaces.write("gateway " + data["Gateway"] + "\n")
        f_interfaces.write("dns-nameservers 8.8.8.8 8.8.4.4")
        f_interfaces.close()

        url =  "http://" + webhost + "/VirtualMachine/SetStatus?id=" + str(data["Id"]) + "&status=NETWORKED"
        data = json.load(urllib2.urlopen(url))

        print "[+] Restarting the server"
        os.system("shutdown -r now")

