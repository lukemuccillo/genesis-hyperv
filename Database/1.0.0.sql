CREATE TABLE [dbo].[VirtualMachines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VMName] [nvarchar](50) NOT NULL,
	[TemplatePath] [nvarchar](255) NOT NULL,
	[Hostname] [nvarchar](100) NOT NULL,
	[SwitchName] [nvarchar](20) NOT NULL,
	[MemoryStartupBytes] [int] NOT NULL,
	[MacAddress] [nvarchar](17) NOT NULL,
	[FQDN] [nvarchar](100) NOT NULL,
	[IPAddress] [nvarchar](17) NOT NULL,
	[SubnetMask] [nvarchar](17) NOT NULL,
	[Gateway] [nvarchar](17) NOT NULL,
	[PuppetMaster] [nvarchar](100) NOT NULL,
	[Status] [nvarchar](20) NULL
)


