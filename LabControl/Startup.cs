﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LabControl.Startup))]
namespace LabControl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
