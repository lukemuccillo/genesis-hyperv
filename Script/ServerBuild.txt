# Change root password
sudo su -
passwd

# Remove the initial user
deluser --remove-home lukem

# Remove App Armor
sudo /etc/init.d/apparmor stop
sudo update-rc.d -f apparmor remove
sudo apt-get --purge remove apparmor apparmor-utils libapparmor-perl libapparmor1 -y

# Install Puppet
apt-get install puppet

# Enable the Puppet agent
puppet agent --enable

# Make puppet check in more often
runinterval=300

# Add script to /etc/network/if-up.d/autostart
# Make the script executable
# Shutdown the server.