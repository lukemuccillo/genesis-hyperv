﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime;
using LabControl.Models;

namespace LabControl.Controllers
{
    public class EventLogger
    {
        private const string EventLogName = "Application";

        private const string EventLogSource = "VMControl";

        public static void Debug(string format, params object[] args)
        {
            var message = String.Format(format, args);

            EventLog.WriteEntry(EventLogSource, message, EventLogEntryType.Information);
        }

        public void Debug(string message)
        {
            EventLog.WriteEntry(EventLogSource, message, EventLogEntryType.Information);
        }

        public void Info(string format, params object[] args)
        {
            var message = String.Format(format, args);

            EventLog.WriteEntry(EventLogSource, message, EventLogEntryType.Information);
        }

        public void Info(string message)
        {
            EventLog.WriteEntry(EventLogSource, message, EventLogEntryType.Information);
        }

        public void Exception(Exception exception)
        {
            var message = String.Format("An error was encountered: {0}\n\n{1}", exception.Message, exception);

            EventLog.WriteEntry(EventLogSource, message, EventLogEntryType.Error);
        }


    }


    public class VirtualMachineController : Controller
    {

        private static EventLogger _logger = new EventLogger();
        //
        // GET: /VirtualMachine/
        public JsonResult Results()
        {
            using (var db = new LabRegisterEntities())
            {
                var results = db.VirtualMachines.ToList();

                var model = results.Select(o => new
                {
                    o.Hostname,
                    o.Id,
                    o.MemoryStartupBytes,
                    o.SwitchName,
                    o.TemplatePath,
                    o.VMName
                });

                _logger.Info("Sending all results");

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetMacAddress(UpdateMacAddressModel model)
        {
            using (var db = new LabRegisterEntities())
            {
                var definition = db.VirtualMachines.Single(o => o.Id == model.Id);
                _logger.Info("Setting Mac Address on {0} to {1}", definition.VMName, model.MacAddress);
                definition.MacAddress = model.MacAddress;
                db.SaveChanges();
                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetStatus(UpdateStatusModel model)
        {
            using (var db = new LabRegisterEntities())
            {
                var definition = db.VirtualMachines.Single(o => o.Id == model.Id);
                _logger.Info("Setting Status Address on {0} to {1}", definition.VMName, model.Status);
                definition.Status = model.Status;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDetailsByMacAddress(string id)
        {
            using (var db = new LabRegisterEntities())
            {
                var mac = id.ToUpper();
                var definition = db.VirtualMachines.Single(o => o.MacAddress == mac);
                _logger.Info("Request for details: {0}", definition.VMName);

                var model = new
                {
                    definition.Id,
                    definition.Hostname,
                    definition.VMName,
                    definition.SubnetMask,
                    definition.IPAddress,
                    definition.FQDN,
                    definition.Gateway,
                    definition.PuppetMaster,
                    definition.Status
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
	}

    public class UpdateStatusModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }

    public class UpdateMacAddressModel
    {
        public int Id { get; set; }
        public string MacAddress { get; set; }
    }
}