
$hyperVRootFolder = "D:\LUKE_LAB"

$allDefinitionsUrl = "http://labcontrol.skycoder.com.au/VirtualMachine/Results"

$response = Invoke-WebRequest -Uri $allDefinitionsUrl -Method GET
$definitions = ConvertFrom-Json $response.Content

New-Item $hyperVRootFolder -ItemType Directory -Force | Out-Null

$newDefinitions = @()

foreach ($server in $definitions) {

    $nbProcessors = 1
    $vmName = $server.VMName
    $switchName = $server.SwitchName
    $mbRam = $server.MemoryStartupBytes * 1MB

    Write-Host "[+] Definition: $vmName Processors: $nbProcessors Switch: $switchName RAM: $mbRam" -ForegroundColor Green
    $vmPath = Join-Path $hyperVRootFolder $vmName
    $vhdPath = Join-Path $vmPath "Virtual Hard Disks" 

    $systemVHDPath = Join-Path $vhdPath ("{0}_0.vhdx" -f $vmName)
 
    #Create VM Container folder and copy template VHD
    Write-Host "[+] Creating folder for VM resources: $vmPath"
    New-Item -ItemType Directory -path $vmPath | Out-Null

    Write-Host "[+] Creating folder for VHDX's: $vhdPath"
    New-Item -ItemType Directory -path $vhdPath | Out-Null
 
    Write-Host "[+] Copying Template VHD to Virtual Machine"
    Write-Host "[i] Source: $templateVHDPath" 
    Write-Host "[i] Destination: $systemVHDPath" 
    Copy-Item $templateVHDPath $systemVHDPath | Out-Null
    Write-Host "[+] Template successfully copied"
 
    Write-Host "[+] Creating Virtual Machine $vmName"
    $vm = New-VM  -Name $vmName -NoVHD -Path $vmPath `
                  -MemoryStartupBytes $mbRam -SwitchName $switchName -Generation 1 
 
    Write-Host "[+] Setting processor count to $nbProcessors"
    Set-VMProcessor -VM $vm -Count $nbProcessors

    Write-Host "[+] Setting MAC address to dynamic"
    Set-VMNetworkAdapter -VM $vm -DynamicMacAddress

    Write-Host "[+] Starting VM for the first time"
    Start-VM -VM $vm

    Write-Host "[+] Waiting for VM to come online"
    $isUp = $false

    while (-not $isUp) {

        Write-Host "`r[+] Checking VM State on $vmName" -NoNewline
        $vm = Get-VM -Name $vmName
        Write-Host "`r[+] Checking VM State on $vmName - $($vm.State)"

        if ($vm.State -ieq "Running") {
            $isUp = $true
            break
        }

        Sleep -Seconds 1
    }

    Write-Host "`r[+] Getting current VM MAC address" -NoNewline
    $firstMacAddress = $vm.NetworkAdapters[0].MacAddress

    $formattedMacAddress = ""
    for($i = 1; $i -le $firstMacAddress.Length + 1; $i++){
        
        $formattedMacAddress += $firstMacAddress[$i - 1]

        if ($i % 2 -eq 0 -and $i -lt $firstMacAddress.Length){
            $formattedMacAddress += ":"
        }
    }

    Write-Host "`r[+] Getting current VM MAC address - $formattedMacAddress"

    Write-Host "[+] Adding Mac Address $formattedMacAddress to definition for server $vmName"
    $url = "http://labcontrol.skycoder.com.au/VirtualMachine/SetMacAddress?Id=$($server.Id)&MacAddress=$formattedMacAddress"
    Invoke-WebRequest -Uri $url -Method GET | out-null

    Write-Host "[+] Shutting down VM $vmName"
    Stop-VM -VM $vm -TurnOff

    $isDown = $false

    while (-not $isDown) {

        Write-Host "`r[+] Checking VM State on $vmName" -NoNewline
        $vm = Get-VM -Name $vmName
        Write-Host "`r[+] Checking VM State on $vmName - $($vm.State)"

        if ($vm.State -ieq "Off") {
            $isDown = $true
            break
        }

        Sleep -Seconds 1
    }

    Write-Host "[+] Attaching Hard Disk $systemVHDPath"
    Add-VMHardDiskDrive -Path $systemVHDPath -VM $vm -ControllerType IDE

    Write-Host "[+] Bringing Virtual Machine Online"
    Start-VM -VM $vm
}

